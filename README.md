Federated Friends Locking
=========================


## Quickstart

Install the dependencies. The quickest is
```
sudo pip install -r requirements.txt
```
but doing it manually with `pacman` or `apt-get` might work better for you.

Then run
```
python blog.py
```
and go [here](http://localhost:5000).


The rest of this README is a mess right now. Skip it unless you want to see my thought process.


# Explanation

Semi-public posts (blog or otherwise) are very useful, socially.
They allow engaging in the discussions on the public web,
without all the riskiness of being totally public,
and having all your history logged for inspection by doxxers or just jerks.

There are two methods of semi-public posts:

- have a pseudoanonymous name, and guard it carefully or dump it quickly (e.g. tumblr) -- hard to do right!! if the data is public it's still public, and there's fuckers like [gnip](https://gnip.com) and [that one in Utah](https://en.wikipedia.org/wiki/Utah_Data_Center) and the less malicious but still troubling [Internet Archive](https://web.archive.org) or the various search engine caches who archive everything anyway
- have everyone on a central server (Livejournal, Facebook, G+)
- arrange a complicated federated-subscription thing, where users follow each other and then optionally can put followers into groups (Diaspora)
- don't (Twitter)

I have a simpler idea:

 Get people ("subscribers") to authenticate to you ("author") *once*.
 From the subscribers' perspective, instead of getting an account (in particular, instead of getting a password) to remember, they just get a customized RSS feed
   Instead of authing the feed each time, it is downloadable anywhere, but protected by TLS+a long random string, the way Google docs are.
 from your perspective, you get a notification when you get a new follower,
 just like normal, and you can accept them or not. when you accept them they get put into a default group ("Subscribers"?) and you have the option of adding others ("Friends", "Family", etc. the normal)
 but there is a subtle difference in what "accept" means from elsewhere:
 - the set of people you've accepted as friends is not public
 - people's subscription still behaves even without you accepting them
 - "accepting" doesn't mean "follow back" (because there's nothing standard attached to their authentication the way there is when you follow back within a centralized server)


Identities
----------

For us an identity, in general, is `[type]:[id]`, where the details of `[id]` depend on `[type]`.
This is a list of identity providers we plan to support. By being extensible like
this we should be future-proofed against all this shifting too much, though supporting
additions and deprecations via upgrades will need to be a major priority.

We need to make it clear to people deploying FFL that different auth methods have different security strengths!

* local: -- classic local sign on, with a username/password
* OAuth2
    * Each OAuth2 provider requires a separate, painful, set up effort on the sysadmin's part,
    and a bit of extra glue code because OAuth2 **is not for authentication**, but rather it
    gives *authorization* which then, per-provider, can usually be used to get authentication too
    so each of they are effectively separate providers;
    * Caveat: OAuth2 was meant for authorization: allow; the most common use is for authentication though, which actually requires
    * Caveat: it's [not very secure](http://homakov.blogspot.ca/2012/07/saferweb-most-common-oauth2.html)
    * facebook:
        * facebook, additionally, hands out unique IDs to each verifier, to prevent sites tracking facebook users cross-site; so you cannot share your ACLs across sites or with friends if you take Facebook accounts.
    * google:
    * twitter:
    * github:
* tel: (SMS / Phone call)
* mailto: (Email)
* xmpp:
* [keybase:](https://keybase.io/)
* A website address
* A domain name -- letsencrypt uses this
* OpenID -- again, a domain name
    * uh there's a few revisions of this... which are we supporting?
* Some sort of Yubikey / OTP system?
* [IndieAuth](https://indieauth.com/) from http://indiewebcamp.com/
    * Uses a web URL as an identity, but in turn fans out to other identity providers to actually verify you are someone
    * The goal was to put identity under something you control (your domain name) while keeping the complication of authentication on someone else
* anonymous: -- this by definition has an empty string
* [WebID](http://webid.info/)
    * The W3C has [identity](https://www.w3.org/2005/Incubator/webid/spec/) + [access control](https://www.w3.org/wiki/WebAccessControl) in progress, but it doesn't seem to have much traction
* [http://browserauth.net/](http://www.browserauth.net/)
* [Thali](http://thaliproject.org/)
* [Hubzilla](https://hubzilla.org/help/about/about_hubzilla) has [a protocol](https://hubzilla.org/help/developer/api_zot) which promises distributed, federated identity
* [Google Upspin](https://www.upspin.io/) is a project from Rob Pike (of Unix, Plan 9, and now Google) and friends to do a distributed identity-based and filestore
* https://storj.io/ is a distributed storage based on bitcoin-style blockchain identity, which is maybe a littttle out of scope but it's interesting to think about
* BrowserID (aka Mozilla Persona) ([dead](https://wiki.mozilla.org/Identity/Persona_Shutdown_Guidelines_for_Reliers))
* Bitcoin addresses
* RSA keypairs (see also: bitcoin addresses; keybase.io)

These identities are typically terse; while emails are (mostly) human-readable (`first.last@gmail.com`),
facebook's look like `facebook:2942842424444442` and that's bad UX.
Therefore, to get this off the ground we need to install a
[petnames](http://www.skyhunter.com/marcs/petnames/IntroPetNames.html) system.
Some some identity types come with a petname available (e.g. most of the OAuth2s,
XMPP if you can [access the user vcard](https://xmpp.org/extensions/xep-0153.html#retrieve)),
and others will need to be hand-entered.
Probably when someone signs in for the first time the owner of the ACLs needs to
get pinged (probably via email) that someone is interested in them and here
is the (petname, identity) pair they are claiming and would you like to edit the petname?

*To research*: what if someone you know signs in with multiple identities?
Do we have some sort of grouping system?



Authentication Methods
----------------------

An auth method is an algorithm that proves ownership of an Identity.
In some ways authentication is the same as identity: in practice if you can prove ownership you are that owned thing.
But for us the difference is that authentication is an algorith, while an identity is the output of that algorithm;
an identity is something the user can write into an ACL.

* OAuth2
* OpenID
* [SASL](https://en.wikipedia.org/wiki/Simple_Authentication_and_Security_Layer)
* imap: (e.g. Radicale [can use this](http://radicale.org/user_documentation/#idldap-authentication), though it requires handing over your email password to the server, so that's probably out of the question)
* [LDAP](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)
* [Kerberos](https://en.wikipedia.org/wiki/Kerberos_%28protocol%29)
* [AD](https://wiki.samba.org/index.php/Setting_up_Samba_as_an_Active_Directory_Domain_Controller)


Enforcing ACLs
==============

At runtime, how do we actually intercept HTTP requests to check ACLs?

The classic option to enforce ACLs on the web is by building them by hand,
or maybe getting your framework to do it for you (e.g. [django.contrib.auth.User](http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/relayd.conf.5)).
This is fine if you are invested in your chosen framework,
but I am not targetting FFL at that userbase; it ties you to a particular framework
and uses a local identity whereas I explicitly want distributed identity (exception: [python-social-auth](http://python-social-auth-docs.readthedocs.io/en/latest/) but it's terrible).

A more clever option is X-Sendfile, where we run a small CGI program which checks permissions (well, it can actually do whatever it likes)
and then instead of loading and sending the file itself, it sends back a pointer
to the file to send via the X-Sendfile header; so long as the real files don't exist
within the web root, this means the only way to get them is by going through the CGI program,
but then after that the server, which is presumably optimized for sending static
files, ideally [using sendfile()](http://man7.org/linux/man-pages/man2/sendfile.2.html)
(unlike FFL), which the server picks up.

The disadvantage of this is that it, while it doesn't tie you to specific web frameworks,
it does tie to you specific web servers because only some webservers speak it.
* [nginx](https://www.nginx.com/resources/wiki/start/topics/examples/x-accel/)
* [apache](https://tn123.org/mod_xsendfile/)
* [lighttpd](http://redmine.lighttpd.net/projects/lighttpd/wiki/Docs_ModFastCGI#X-Sendfile)
* [python API](http://pythonhosted.org/xsendfile/)


*To research*: can OpenBSD's [httpd](http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/httpd.conf.5)
can somehow be hacked into supporting FFL too?
Its compatriot [relayd](http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/relayd.conf.5)
seems like it almost supports all the features we want; maybe the CGI sets a special signed header
that only relayd sees, essentially saying "this request is approved".


Initial Authentication
----------------------

Also, ideally authentication is handled by an identity provider, so that users don't need to sign up for 
I think how I'm going to do this is python-social-auth
which is  unifying wrapper for OpenID, OAuth 1 and 2, and email authentication.
 STUMBLING BLOCK:
  - OAuth a) is *not* an authentication protocol, it's an *authorization* protocol
             it is meant for handing out capabilities, in the form of session tokens, so that mashup sites can e.g. pull your google calendar, access your twitter DMs, pull your profile data off facebook...
               people abuse it into being an authentication protocol (see: Soundcloud, Disqus, Livejournal)
               but it's really unweildy for this, and the original core author has bailed and railed against it
               nevertheless, it currently dominates "social login" because Facebook, Twitter, and Google support it and Facebook and Twitter *don't* support OpenID
          b) because it's meant for making apps interact, *every app must have app keys from *each* provider
             and an "app" means "a website"
             so every person who wants to use OAuth to log their friends into their blog must create a developer account at *each* site they want and deal with that bureaucracy
             which is bulllllshit

I think for a first draft, I will simply do use email auth:
- prove that you own an email (as sketchy as that is), and get the feed from that.
   -> need to protect against replays and whatnot; if you can do it stateless, even better

Or maybe just a single string without verification? after all, auth only happens once, so ToFU??


Repeat Authentication
---------------------

The goal is to avoid making the reader deal with auth as much as possible (if I make my friends sign in to read my blog none of them will read my blog)
So repeat auth is entirely via the 

 idea one: embed auth tokens (or session cookies) in URLs
 idea two: somehow have a landing page which puts session cookies 


idea one is stateless, and matches exactly how 100 works
idea two


References
==========

* https://www.digitalocean.com/community/tutorials/how-to-structure-large-flask-applications <----- kthx digitalocean :)
* ugh why http://fewstreet.com/2015/01/16/flask-blueprint-templates.html

### Static Site Generators

* https://github.com/getpelican/pelican/
* Jekyll??
 - comments in Jekyll:
   http://www.hezmatt.org/~mpalmer/blog/2011/07/19/static-comments-in-jekyll.html 
* To combine FFL with a static generator look into [X-Accel-Redirect](https://www.nginx.com/resources/wiki/start/topics/examples/x-accel/) and it's non-nginx equivalents.
    * otherwise you're stuck on Flask or, like the sprawling mass that is python-social-auth, you're stuck supporting multiple frameworks, and you're definitely stuck on python
    * 