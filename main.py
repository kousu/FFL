#!/usr/bin/env python3

from __future__ import absolute_import
from __future__ import print_function

import os
import yaml

from subprocess import Popen

# libraries
from flask import *
from flask_login import *
from flask_omniauth import *
from users import *

import traceback


app = Flask(__name__)
#app.config["EXPLAIN_TEMPLATE_LOADING"] = True

# Pull in config from the OS environment vars
for conf in [
    "OMNIAUTH_PROVIDERS",

    #"OMNIAUTH_XMPP_USERNAME",
    #"OMNIAUTH_XMPP_PASSWORD",

    "OMNIAUTH_FACEBOOK_OAUTH_ID"
    "OMNIAUTH_FACEBOOK_OAUTH_SECRET"
    "OMNIAUTH_GITHUB_OAUTH_ID"
    "OMNIAUTH_GITHUB_OAUTH_SECRET"
    "OMNIAUTH_GOOGLE_OAUTH_ID"
    "OMNIAUTH_GOOGLE_OAUTH_SECRET"
    "OMNIAUTH_TUMBLR_OAUTH_ID"
    "OMNIAUTH_TUMBLR_OAUTH_SECRET"
    "OMNIAUTH_TWITTER_OAUTH_ID"
    "OMNIAUTH_TWITTER_OAUTH_SECRET"

    # TODO: Flask-Mail already has settings for these..
    # also you could just make sure mail(1) works for the user Flask is run as and then you're golden.
    ]:
    app.config[conf] = os.environ.get("FLASK_" + conf, "")

app.secret_key = os.environ.get('FLASK_SECRET_KEY', os.urandom(32))

login_manager = LoginManager()
login_manager.init_app(app)
#login_manager.login_view = "auth_manager.login" # or "/login" ?? # TODO: test me
auth_manager = Omniauth(app)

@user_logged_in.connect
def save_user(app, user):
    user_save(user)


@user_first_login.connect
def friend_request(app, user):
    app.logger.info("%s has followed you.", user)
    # every user is, by default, a Follower
    group_add('followers', user)

    # notify the site owner of their new follower
    Popen([os.path.join(os.path.dirname(__file__), "flask_omniauth", "email"),
           "owner@example.com", # TODO: flask config var
           "%s has followed you" % user.id,
           "%s has followed you. They have been placed into %s."
           "\r\n\r\nManage their groups at %s"
           % (user.id,
              ", ".join(e.title() for e in groups(user)),
              "/users/" + user.id,
              #url_join(request.url, url_for("users",user.id)) #TODO: figure out a way to make this a full URL;
             )
          ])


@login_manager.user_loader # callback flask_login.LoginManager.user_loader
def load_user(userid):
    try:
        return user_load(userid)
    except Exception as exc:
        flash("Sorry, something's gone wrong. We were unable to log you in.")
        current_app.logger.error(traceback.format_exc())
        logout_user()
        # XXX bug: if you manage to construct an invalid user account cookie
        # and then call logout_user in here, you get an infinite recursion
        # The Flask-Login flow is supposed to be:
        # logout_user removes the state, especially session['user_id']
        # it calls reload_user()
        # reload_user() sees that there is no state and constructs an AnonymousUserMixin
        # But somehow reload_user is finding that there is, in fact, a user_id to be gotten
        #app.logger.error("oh no")


@app.route('/')
def index():
    #flash("this is a demonstration flash") #DEBUG: flashes are tricky to style because they only last one page reload
    app.logger.debug("You are: %s", current_user)
    app.logger.debug("Session = %r", session)
    return render_template("index.html")


@app.route('/<path:page>')
def static_file(page):
    try:
        if not (current_user.is_authenticated and current_user.id in acl(page)):
            abort(403)
    except FileNotFoundError:
        abort(404)

    # return the static file
    return flask.send_file(page)


if __name__ == '__main__':

    if all(os.access(f,os.O_RDONLY) for f in ["localhost.crt","localhost.key"]):
        import ssl
        t = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        t.load_default_certs() # OAuth2 specs that you MUST use TLS, because for simplicity it doesn't do any crypto itself. This is probably a good idea, but it does make testing tricky.
        t.load_cert_chain("localhost.crt","localhost.key")
    else:
        t = None
    
    app.run(use_reloader=False, host="0.0.0.0", ssl_context=t)



if False: # commented-out hack
    ############################################
    # This code implements a mock user database,
    # which just saves users to a cookie.
    #
    # It seems useful, but I don't know how to
    # package it up for reuse. Particularly,
    # it's going to crash into any other user_loader
    # you could muster, so using this seems
    # to be mutually exclusive with having a real
    # login system of any kind.
    ############################################

    @user_logged_in.connect # signal flask_login.user_logged_in
    def save_user(app, user):
        """
        On login, persist the user object.

        In this implementation, we save to the session cookie directly, in lieu of figuring out a database.
        """

        #app.logger.info("save_user(%r, %r)", args, kwargs)
        app.logger.info("save_user(%r)", user)

        if 'user' in session:
            app.logger.warn("updating saved user object %r", user)
        session['user'] = yaml.dump(user)

        # Another thing to do is to send a first-save ping,
        # (which you can't do without some sort of database to check against..)
        # which is tricky to factor nicely because you can't know

    # Since I don't have a database set up, store the entire user in the session cookie.
    # TODO: explore overriding lower layers of flask and flask-login so that, rather than storing session['user_id'], you store session['user'] =, and rather than user_loader being given a userid, it just loads directly
    @login_manager.user_loader # callback flask_login.LoginManager.user_loader
    def load_user(userid):
        """
        On page load, restore the user object

        In this implementation, we load from the session cookie directly, rather than use a database.
        """
        app.logger.info("load_user(%r)", userid)

        if 'user' not in session:
            logout_user()
            return

        user = yaml.load(session['user'])
        if not (user.id == userid):
            app.logger.error("session['user_id'] == %r != session['user'].id == %r" % (userid, user.id))

        return user

        # TODO: load the rest of the details from disk

        # reconstruct the user object out of thin-air
        # just using the information in the userid
        #provider, userid = userid.split(":", 1)
        #return User(provider, userid)

    @user_logged_out.connect # signal flask_login.user_logged_out
    def forget_user(app, user):
        app.logger.info("forget_user(%r)", user)
        if 'user' not in session:
            app.logger.warn("user object expected in session cookie at logout time, but it's missing.")
            return
        del session['user']
