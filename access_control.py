
from __future__ import absolute_import
from __future__ import print_function

import sys, os

from .util import chrooted

def acl(file):
    """
    Read and return the access control list for the given file.
    
    ACLs are stored in a flat-file database on disk next to the files they guard:
    for path/to/file.txt the ACL is in path/to/file.txt.acl.
    
    The format is:
    .......
    
    As a special case, .acl files themselves are readable by no one.
    """

    # special case: the .acl files themselves are always hidden
    if file.endswith(".acl"):
        return set()
    
    # protect against directory traversal attacks
    file = chrooted(file)

    # make sure the file actually exists before we try to load its associated ACL file.
    # we have to handle this here because send_file doesn't
    # it ends up raising FileNotFoundError but not translating that to a 404
    # we could probably handle this after checking the ACLs too,
    # but being here is a short-circuit
    if not os.path.exists(file):
        raise FileNotFoundError(file)

    acl = file + ".acl"
    
    # treat a missing ACL file as
    if not os.path.exists(acl):
        return set()

    # finally, read the ACL file 
    with open(acl) as acl:
        return set(line.strip() for line in acl.readlines())

if __name__ == '__main__':
    print("No tests")
