#!/bin/sh
# run a standalone, unprivileged, nginx
# like many servers, nginx was never designed with this in mind
# there's nothing that stops it doing it except that it
# assumes a lot of things about config paths that we need to 
# tediously override.

# where this file is
HERE=$(dirname $0)

# get the absolute path of a a path
abspath() {
    # cd + pwd is the only crossplatform way
    # so we do this in a subshell to avoid screwing up the current path
    (cd $1; pwd)
}
CONF_ROOT=$(abspath $(mktemp -d .nginx_XXXXXXXX))
mkdir $CONF_ROOT/tmp

# write the config file

cat > $CONF_ROOT/mime.types < $HERE/mime.types
cat > $CONF_ROOT/fastcgi_params < $HERE/fastcgi_params

cat > $CONF_ROOT/nginx.conf <<EOF
error_log $CONF_ROOT/tmp/error.log;
# This nginx config runs without sudo
# run with
# mkdir ./tmp; nginx -p ./ -f path/to/this/nginx.conf
# this will make nginx server files from the working directory

load_module /usr/local/libexec/nginx/ngx_mail_module.so;
load_module /usr/local/libexec/nginx/ngx_stream_module.so;


worker_processes  1;
daemon off;

error_log  /dev/stderr;

pid        $CONF_ROOT/tmp/nginx.pid;


events {
    worker_connections  1024;
}


http {
    # this should be at the same level as error_log, for our purposes, but nginx disallows that
    access_log  /dev/stdout;

    # this pile of crap is because nginx is defaults to
    # using the prefix /var/tmp/nginx/ for all of these,
    # which is *not* the same prefix as set with nginx -p,
    # and if we're trying to run unprivileged this blocks us
    # because that directory is probably owned by root.
    client_body_temp_path $CONF_ROOT/tmp/client_body/;
    proxy_temp_path $CONF_ROOT/tmp/proxy/;
    fastcgi_temp_path $CONF_ROOT/tmp/fastcgi/;
    uwsgi_temp_path $CONF_ROOT/tmp/uwsgi/;
    scgi_temp_path $CONF_ROOT/tmp/scgi/;
    
    # Content-Type: header
    include       $CONF_ROOT/mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    server {
        listen       8080;
        server_name  localhost;

        location / {
            # this + -p . means that that webroot is *the current directory*
            root   ./;
            index  index.html index.htm;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        location ~ \.php$ {
            proxy_pass   http://127.0.0.1:12448;
        }

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        $CONF_ROOT/fastcgi_params;
        #}

    }


}
EOF

# despite everything above this *still* always tries to open /var/log/nginx/error.log
# they say this is to ensure you never miss errors, especially if those errors
# then go on to break your logging,
# but that's just becaues they don't know how to componentize their OS.
nginx -p . -c "$CONF_ROOT"/nginx.conf

rm -r "$CONF_ROOT"
