#!/usr/bin/env python3
"""
TODO:

[ ] CSRF!! (WTForms!)
[x] Patch bootstrap-social to cover pseudoanon, email, user/pass, and phones
[ ] Glitches:
 - tumblr (on OAuth1) and google (on OAuth2) both for some reason *always* re-prompt the user
   but the other sites log you in smoothly
   if google is always going to reprompt, why does it have {access_type="offline", approval_prompt="force"} as options?
    -- I think maybe if I pass ?access_type=offline then google will save the thing? maybe? hm.
[ ] Support permission scopes (---do I want to do this? really? if the goal is social sign in then I should never need any but the default scope)
[ ] Auto-load compliance fixes
  # TODO: [e for e in dir(requests_oauthlib.compliance_fixes) if e.endswith("compliance_fix")]
[ ] Save avatars server-side and serve them from there (to avoid creating a tracker inadvertantly)
  -- Make sure to think through the privacy implications for subscribers.  If you copy their avatar they lose control of it!
  -- in fact, Disconnect.me blocks the Twitter avatars specifically because they are also trackers
[/] Support other web frameworks besides Flask
  -- nope: this is a Flask. However, I'll set up a thin static-site-service Flask app so that you don't have to run.
[ ] Cheat: translate Facebook app-scoped UIDs to global UIDs
  -- apparently Facebook didn't actually make app-scoped IDs local to each app
  and they provide "https://www.facebook.com/app_scoped_user_id/<id>
  the catch is you have to be logged in to follow it
  but you can use *any* account to follow the link and get jumpped over to facebook.com/<username> (and by screen-scraping you can also find the global user ID)
"""

from __future__ import absolute_import
from __future__ import print_function

import sys, os

# this boilerplate derived (poorly) from
# https://www.python.org/dev/peps/pep-0366/
# it makes relative imports work when this file is run directly
if __name__ == "__main__" and __package__ is None:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    import flask_omniauth; del flask_omniauth
    sys.path.pop()
    __package__ = "flask_omniauth"


# core
import sys, os

from collections import OrderedDict

# libraries
import flask
from flask import *
from flask_login import *

from jinja2 import *

# local
from . import users
from . import providers
from .util import *



class Omniauth(object):
    """
    TODO: Flask really prefers extensions that add routes to do it as a flask.Blueprint.
    """
    def __init__(self, app):
        self.app = app
        if not getattr(self.app, 'login_manager', None):
            raise ValueError("You must configure Flask-Login before configuring Flask-Omniauth")
        #self.app.omniauth = self # XXX necessary?

        _providers = app.config.get("OMNIAUTH_PROVIDERS", "").split() #XXX case-sensitivity?
        if not _providers:
            _providers = providers.__all__
        # load all providers
        _providers = [getattr(providers, p) for p in _providers] # get the class
        _providers = OrderedDict((p.__name__.lower(), p) for p in _providers) # tag it by its name for login()
        #_providers = [p() for p in _providers] # instantiate the class
        #_providers = OrderedDict((type(p).__name__.lower(), p) for p in _providers) # tag it by its name for login()
        self.providers = _providers

        self.app.logger.info("Available authorization providers:\n%s",
                             str.join("\n", ("* %s" % p for p in self.providers)))

        self.app.route("/login/")(self.login)
        self.app.route("/login/<provider>", methods=["GET","POST"])(self.login)
        self.app.route('/logout', methods=["GET","POST"])(self.logout)
        self.app.errorhandler(403)(self.access_denied)

        # hack: make resources from this package loadable by the caller app, so that the login forms actually render.
        # the sanctioned way to do this sort of thing is to write this all as a Blueprint, though :/
        # Blueprints are wrong though. They are factored wrong! They are essentially micro-apps,
        # so why not make the whole thing so that apps can be made of apps? So I don't want to use them. wahhh.
        # And anyway a blueprint requires a unique mountpoint and I don't want that: I want omniauth to invade
        # your URL space and do nice things to it.
        # Maybe what I have to do is make a Blueprint that handles the static files and templates
        # but wrap it up in an extension which does; that is, the extension, right here in __init__,
        # also registers a Blueprint at, say, ".flask_omniauth/"

        # Step 1: templates
        # wrap the existing jinja template loader
        # with one that falls back
        # Flask does something different with Blueprints: it has an explicit checking of self.jinja_loader followed by a loop over self.blueprints -> loaders
        #self.app.jinja_loader = ChoiceLoader([
        #    self.app.jinja_loader,
        #    PackageLoader(__package__), # at least jinja is smart here
        #    ])
        # this should do the same thing, but only if the jinja_loader is a FileSystemLoader
        import pkg_resources
        self.app.jinja_loader.searchpath.append(os.path.join(pkg_resources.get_provider(__package__).module_path, "templates"))

        # Step 2: static files
        # Strangely, there doesn't seem to be any sort of search path for static files like there is for jinja loaders
        # this requires rewriting everywhere we use static files -- mostly in templates -- to say url_for("omniauthstatic", ...) instead of url_for("static", ...)
        self.app.route("/.omniauth/static/<path:filename>")(self.omniauthstatic)

    def omniauthstatic(self, filename):
        # this is a hack because Blueprints are the Right Way
        # don't expect it to last
        path = chrooted(filename)
        import pkg_resources
        path = os.path.join(pkg_resources.get_provider(__package__).module_path, "static", path)
        try:
            return flask.send_file(path)
        except:
            return "Not Found", 404 # TODO: parse all possible exceptions and give appropriate codes
        #return self.app.send_static_file(path)

    def login(self, provider=None):
        if provider is None:
            return render_template("flask_omniauth/login.html", providers=self.providers.values())
        else:
            if provider != provider.lower():
                return "Provider names must be lowercase", 400
            if provider not in self.providers:
                return "Unsupported login provider.", 404
            # TODO: I'd prefer to keep the login_user calls here
            # solution: use promises (duh) aka continuation-passing-style:
            # hand handle a callback for when it actually
            # that, or, have handle return: if it returns a thing, we log it in
            ret = self.providers[provider].handle()
            if isinstance(ret, users.User):
                login_user(ret)
                return redirect("/")
            else:
                return ret


    def logout(self):
        """
        # TODO: implement CSRF protection
        # ( this requires... WTForms? I think? )
        """
        logout_user()
        return redirect("/")

    def access_denied(self, error):
        # TODO: add a passthrough to bounce you back to where you were trying to go once you log in
        # and also some UI explaining "you don't have perms, sorry"
        return redirect(url_for("login"))
        #return render_template('flask_omniauth/login.html'), 403
