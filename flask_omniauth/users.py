"""
User database
"""

from __future__ import absolute_import
from __future__ import print_function

from flask_login import *

#from . import providers # under python2 this causes a weird recursive import failure, because this module hasn't been loaded to sys.modules yet, whereas in python3 it gets loaded to sys.modules before any of its code is run. i guess.
# my hack solution is to move the import to the one place it's needed down below. but there's gotta be something better..

class User(UserMixin):
    """
    Base class for Represent a user, a single person, or at least a single identity.
    This class does not enforce any sort of constraints: *SECURITY IS NOT HERE*. You must make sure not to hand out User objects unless you have some external sort of auth.
    
    * provider is a string identifying the identity server ("github", "facebook", these should be pretty globally unique: use https://github.com/lipis/bootstrap-social/ as a guideline)
     * you could also use non-providers, like "anonymous", "local", "mailto" or "xmpp", so long as you have a way for the user to prove they own the address
    * userid uniquely identifies the user within the world of `provider`
    * .urn combines these into a URN as "$provider:$id"
    
    The optional extra fields are:
    * username is the username someone has on a site; this is typically different than their ID number, and lots of sites even allow changing it.
    * display_name is a full name, which should be displayed
    * ~avatar should be a URL (note: data: urls with encoded images are allowed here, if that's what the site gives or if you want to rip the image locally.
    Feel free to extend this class with more details if appropriate.
     extension ideas:
        - something that holds onto the OAuth token
    """
    
    def __eq__(self, other):
        return self.__dict__ == other.__dict__
    
    def __init__(self, provider, userid, username=None, display_name=None, avatar=None):
        assert provider
        assert userid
        # coerce "" -> None
        username = username or None
        display_name = display_name or None
        avatar = avatar or None

        # record data
        self.provider = provider
        self.userid = userid
        self.username = username
        self.display_name = display_name
        self.avatar = avatar

    
    @property
    def id(self):
        """
        Return a globally unique ID string for this user.
        This is what should be used as a primary key in a database
        and what flask_login.user_loader should use to reload a user from
        """
        return "%s:%s" % (self.provider, self.userid)
    
    @property
    def url(self):
        from flask_omniauth import providers
        p = [p for p in dir(providers) if p.lower() == self.provider.lower()]
        assert p, "Unknown self.provider == %r" % (self.provider, )
        #assert len(p) == 1, "Multiple providers matched %r" % (self.provider,)
        p = p[0]
        p = getattr(providers,p)
        return p.link(self.userid)

    def __str__(self):
        if self.display_name:
            return str(self.display_name)
        else:
            return self.id
