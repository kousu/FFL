from __future__ import absolute_import
from __future__ import print_function


from flask import *

from .base import Provider
from .. import users

class Local(Provider):
    # TODO
    # this would be the username/password option, I guess
    # Unfortunately, this depends on having a *further* password backend
    #  which could be PAM or LDAP or something in a custom database
    name = "Username/Password"
    icon = "sign-in"
    
    @staticmethod
    def link(id):
        return # I want local users to be like, https://site.com/users/<userid> but maybe that won't work because reasons
        #return url_for("user", userid=id)
    
    @classmethod
    def handle(self):
        
        if request.method=="GET":
            return render_template("flask_omniauth/login_local.html", mode=request.args.get("mode","login"))
        elif request.method == "POST":
            if not self.verify(request.form['id'], request.form['pass']):
                flash("Login incorrect.")
                return redirect(request.url)
            
            name = None
            # name = lookup_id_in_database(request.form['id'])
            return users.User(self.__name__.lower(), request.form['id'], name)
    
    @staticmethod
    def verify(user, password):
        """
        override this to plug in your actual backend
        """
        return True #debug
        return False
