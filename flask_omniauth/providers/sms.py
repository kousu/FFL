
from __future__ import absolute_import
from __future__ import print_function

import binascii
import time
from subprocess import Popen

import oath
from flask import *

from .base import Provider
from .. import users

import os
import pkg_resources
SMS = os.path.abspath(os.path.join(pkg_resources.get_provider(__package__).module_path, "..", "./sms"))


def totp_key(provider, id, secret_key):
    """
    Generate a key for use with totp, for the given user id
    Normally with TOTP, you're supposed to use a random string (e.g.)
    or perhaps the user's password
    but the point of this system is to prove that someone is who they say,
    which means we don't have (or, it's really tricky to have) state like that.
    and we can't just give out a key because that ruins it
    so this concats `id` to the flask secret key and writes it in hex (which oath.totp() requires)
    """
    return binascii.hexlify(("%s:%s:%s" % (provider, id, secret_key)).encode('utf-8')).decode('ascii')



class SMS(Provider):
    """
    An phone-number identity provider. Your identity is like "sms:+15551118888"

    The authentication used here is TOTP, with an intentionally weak but short-lived
    code to make copying from a cellphone less tedious. Also the phone network has basically zero security.
    Because of that, anyone using this provider should be viewed with a grain of salt.

    FIXME: scrap instructions for getting this going
    Setting up on Vitelity.net is tricky.
    0) login to https://portal.vitelity.net
    1) provision a number (a "DID")
    2) find the number's status page
    3) click "add SMS" on it (whatever Vitelity has renamed it now)
    4) try the XMPP interface: login as <number>@s.ms and try sending a message to <othernumber>@sms (note: they dropped the period in the vhost name, just to be confusing)
      if that doesn't work, wait an hour
      if it still doesn't work, open a support ticket, and wait
    5) Configure your account for API access from the IP you're sitting behind (link???)
    
    Thought they they provide an `API <http://apihelp.vitelity.net/#sendsms>` they say "Bots and scripts not allowed" on the SMS config page
    this is probably just covering themselves against running up against CTIA regulations:
      http://www.experian.com/blogs/marketing-forward/2013/01/02/sms-compliance-what-you-dont-know-can-hurt-you/
    but, so long as you're not /spamming/ (and auth is hardly spam) you should be legal. probably. IANAL.
    or you could just set up an xmpp client (`sj? <https://github.com/younix/sj>`). a layer of indirection, but arguably faster to config.
    
    other providers:
    - Clickatell
    - SMSGlobal
    - Twilio (probably the most mature, at the moment)
    - les.net
    TODO: figure out a way to make sure these requests are rate-limited (besides waiting for your SMS prepaid credits to run out)
    """
    # i.e. SMS
    # <i class="fa fa-mobile"></i> https://fortawesome.github.io/Font-Awesome/icon/mobile/
    name = "Mobile Phone"
    icon = "mobile" #code = "tty" is also good
        
    AUTH_WINDOW = 2*60 # 2 minutes
    
    @staticmethod
    def normalize_id(tel):
        # references: https://en.wikipedia.org/wiki/North_American_Numbering_Plan#Administration
        id = "".join(t for t in tel if t.isdigit())
        id = id[-10:]
        if len(id) != 10:
            raise ValueError("Invalid phone number")
        return id
    
    @staticmethod
    def link(id):
        return id
    
    @classmethod
    def handle(self):
        # this code is confusing because it handles three flows simulatenously
        # it would be a *lot* cleaner as a coroutine, but Flask doesn't do coroutines.
        if request.method == "GET":
            return render_template("flask_omniauth/login_sms.html", mode="get_id")
        elif request.method == "POST":
            
            if 'id' in request.form:
                # first reply: get the SMS number
                session['sms_auth_id'] = self.normalize_id(request.form['id'])
                session['sms_auth_name'] = request.form['name']
            
            
            # generate a OATH key; we use the app key plus the user id, so that a) no one can make the key except us b) the keys are unique to each user
            # potential attack: give a different
            # note! we *don't* give the client the key!!
            key = totp_key(self.__name__.lower(), session['sms_auth_id'], current_app.secret_key)
            
            if 'id' in request.form:
                assert 'oath' not in request.form
                
                # send an SMS
                Popen([SMS, session['sms_auth_id'], "Your code for %s is %s" % (request.url, oath.totp(key,period=self.AUTH_WINDOW))]) #Note: we *don't* wait on this to finish before responding to the user, because sms is slow and the totp code only lasts 30 seconds anyway.
                
                return render_template("login_sms.html", mode="get_oath")
            elif 'oath' in request.form:
                id = session.pop('sms_auth_id')
                name = session.pop('sms_auth_name')
                
                # RATE LIMITING:
                # totp() outputs a 6 digit code, so that's a million options.
                # if you can check 1000 options per second (not unreasonable: a botnet with 1000 members could) you
                # so we need to protect against this
                # this make sure no more than 1 request per second from a single connection
                # TODO: detect how well this works
                time.sleep(1)
                
                #
                passed, _ = oath.accept_totp(key, request.form['oath'], period=self.AUTH_WINDOW)
                if not passed:
                    flash("That oath code didn't work, enter another or <a href='%s'>start again</a>." % (url_for("login",provider=self.__name__.lower()),))
                    return render_template("login_sms.html", mode="get_oath")
                
                return users.User(self.__name__.lower(), id, name)
