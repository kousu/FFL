from __future__ import absolute_import
from __future__ import print_function

class Provider(object):
    """
    Base class for authentication providers.
    """
    name = None
    icon = None #shortcode used to refer to this provider in URLs and CSS and such
     #ah, separate presentation from logic: it might not be possible to use the same code across bootstrap-social and requests-oauthlib and elsewhere...

    @staticmethod
    def link(id):
        """
        generate a contact URL link for the given user using this provider
        """
        raise NotImplementedError
    
    @staticmethod
    def normalize_id(id):
        """
        given a user id, validate and normalize it
        """
        return id
