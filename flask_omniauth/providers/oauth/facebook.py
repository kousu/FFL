from .oauth import OAuth2Provider
from ... import users

class Facebook(OAuth2Provider):
    """
    https://developers.facebook.com/docs/facebook-login and
    https://requests-oauthlib.readthedocs.org/en/latest/examples/facebook.html
    
    To get Facebook working, first you need to set your account to developer mode, then [verify your account](https://www.facebook.com/help/167551763306531#How-do-I-verify-my-developer-account?),
    then register an app at https://developers.facebook.com/apps/
    in the app, click "Add Platform", choose "Web", type a URL (arggggh) to lock the oauth to that URL. However, you *can* set this to localhost.
    """
    auth_url = 'https://www.facebook.com/dialog/oauth'
    token_url = 'https://graph.facebook.com/oauth/access_token'
    
    name = "Facebook"
    icon = "facebook"

    @staticmethod
    def link(id):
        return "https://facebook.com/app_scoped_user_id/%s" %(id,)
    
    @staticmethod
    def whoami(session):
        """
        id: numeric account ID (note! IDs are *app scoped*)
        name: freeform real-name
        
        App-Scoping: <https://developers.facebook.com/docs/graph-api/reference/user/>: " This ID is unique to each app and cannot be used across different apps."
          i.e. the ID this gives is *not* the ID you use in https://facebook.com/profile.php?id=<....>
         to prevent apps (easily) colluding and tracking users.
         you can ask for field "link" but it just gives like https://www.facebook.com/app_scoped_user_id/297049897162674.
             now, that link does in fact send you to the right place but requires being logged in with a personal account, which is a bitch and probably against the ToS.
         (however, it doesn't require being logged in under any particular account: any FB account can follow that link and  get the username, *and* the original ID...which means it's a security hole that they'll probably notice and close within the year)
        """
        profile = session.get("https://graph.facebook.com/v2.5/me?fields=id,name,picture{url}").json()
        # NB: by using /me/picture it might be possible to get a larger image:
        #   https://developers.facebook.com/docs/graph-api/reference/user/picture/
        #   however so far everything i've tried has redirected me back to the 50x50 one, so I'll just live with that
        return users.User("facebook", profile['id'], profile['name'], profile['picture']['data']['url']) #why picture.data.url? why not, says Facebook.
