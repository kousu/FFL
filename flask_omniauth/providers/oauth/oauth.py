"""
OAuth1 and OAuth2 base classes.
"""

import requests.auth
from requests_oauthlib import *
import requests_oauthlib.compliance_fixes

from flask import *

from ..base import Provider
from ...util import urlstrip

class OAuthProvider(Provider):
    """
    OAuth is a piece of overengineered crap.

    Unlike the other providers, OAuth doesn't *authenticate* a user, it *authorizes*.
    That is, it doesn't hand out a proof of identity, it hands out a (cryptographic)
    "capability", which is a token that gives permission to do some action, much like
    a bank draft or a house key.
    For authentication we need glue code that turns capabilities into identities,
    by asking for a capability to ask "who are you?" (= {username, full name, avatar}),
    and then immediately using it; each OAuth provider requires different glue code that hits their custom APIs

    Now, the reason for this design is that it was developed by corps wanting to mashup the data in their silos
    without passing around full credentials between each other (remember when Facebook and Gmail wanted
    you to hand over your Hotmail password so they could onboard you by sniffing your address book? OAuth is safer).
    That makes sense for that use case, but in the overdesign the "login" use case totally got swamped.
    But ironically Facebook, one of the biggest pushers of OAuth, advertises their OAuth API as "social auth" ("get your users to login to facebook; increase conversion rates!") and that's 99% of what people use it for:
    Soundcloud and New York Times and a gazillion other sites which get your Facebook name, photo, and id and use these to let you log in without having to secure their own accounts infrastructure; they don't give a damn about reading your Feed or seeing who your friends are.

    Another way OAuth is a shit is that it demands each client register with each provider. Presumably this helps against brute-forcing,
     but it means that accepting social-auths becomes very expensive: each s
     and it means that it is effectively impossible to set up your own custom OAuth provider

    Another way it's a shit is that it has three or four different algorithms:
     one for web browsers
     one for mobile apps
     one for
    each with different security guarantees(!) which means it's up to each provider to choose which to support

    Another: the CSRF token is *optional*. If you just delete it from the oauth call
    Another: it's not standardized if redirect_url should be stored server-side or passed

    Another: it's super, super, super phishable: it trains people to click "Login With" and then type their *remote* password into the following page; if you stay logged in all the time, this is safe, but if you wipe cookies on closing your browser
    then you are constantly logging in everywhere, and if you don't carefully check the URL and that there's a valid cert to go with it you could simply be getting phished all the time.

    """
    # a useful quirk of OAuth is that the callback URL *can be localhost*, because the auth code is passed via the client
    # you know what OAuth could've done instead of making each site specify these URLs?
    # it could have used meta rel= tags to let autodiscovery happen, the way webmentions{.io} work
    # and the way OpenID, ancient as it is, worked:     <link rel="openid2.provider" href="http://www.livejournal.com/openid/server.bml" />
    auth_url = None
    token_url = None

    @classmethod
    def _credentials(self):
        """
        Return the OAuth application credentials for this provider.

        This must be called within a [Flask application context](http://flask.pocoo.org/docs/0.12/api/#flask.Flask.app_context).
        (hooray for judicious use of globals)
        """
        #TODO: check types here to make sure these values are sensible
        app_id = current_app.config.get("OMNIAUTH_%s_OAUTH_ID" % (self.__name__.upper(),), "")
        app_secret = current_app.config.get("OMNIAUTH_%s_OAUTH_SECRET" % (self.__name__.upper(),), "")
        return app_id, app_secret

    @staticmethod
    def whoami(session):
        """
        Abstract method. Subclasses should implement this to, given
        a requests.Session with the proper auth cookies in place,
        grab user data from the auth provider.

        Since OAuth only covers Authorization but does not standardize
        Authentication, we need this glue method to cover each provider.
        (Though OpenID Connect, which is on top of OAuth, does standardize this)
        """
        assert isinstance(session, requests.Session)
        raise NotImplementedError



class OAuth1Provider(OAuthProvider):
    """
    
    """ 
    # OAuth1 has an extra URL it needs to hit
    request_url = None
    

    @classmethod
    def handle(self):
        """
        
        This code is almost identical to the OAuth2 flow. But it's just different enough making it that it needs to be separate but not so large that it's worth trying to factor.
            
        """
        provider = self #HACKS
        app_id, app_secret = self._credentials()
        
        # this code adapted from https://requests-oauthlib.readthedocs.org/en/latest/examples/tumblr.html
        S = OAuth1Session(app_id,
                          client_secret=app_secret,
                          callback_uri=urlstrip(request.url)) #note: OAuth2 calls renamed this to "redirect_uri", just to complicate your life.
        
        if request.args.get("oauth_token") is None: #<-- FLASK
            try:
                session['oauth_state'] = S.fetch_request_token(provider.request_url)
            except oauth1_session.TokenRequestDenied as exc:
                # TODO: provide an prettier error message here
                return str(exc), 500
            # construct the session-specific auth url at the provider to send the user over to
            auth_url = S.authorization_url(provider.auth_url)
            return redirect(auth_url) #<-- FLASK
        else:
            # restore the state from the first request
            S._populate_attributes(session['oauth_state'])
            del session['oauth_state']
            
            S.parse_authorization_response(request.url) #<-- FLASK
            S.fetch_access_token(provider.token_url)
            
            try:
                return provider.whoami(S)
            except:
                return "You suck", 500


class HTTPNullAuth(requests.auth.AuthBase):
    """
    Workaround for a bug in requests-oauthlib 2.9.1 and below.
    There's this line in OAuth2Session.fetch_token():
      >         auth = auth or requests.auth.HTTPBasicAuth(username, password)
     most providers ignore the Authorization: header when not asked for it, but Google at least is picky
     and will 400 the request with no explanation
     Newer requests-oauthlib now says
      >       if (not auth) and username:
      >          if password is None:
      >            raise ValueError('Username was supplied, but not password.')
      >          auth = requests.auth.HTTPBasicAuth(username, password)
     which is a lot more reasonable.
    
    But 'auth or ....' is really annoying to hack around. I can't just set auth=None or auth="" or something.
    In lieu, This class pretends to be an auth thingy but actually is a no-op
    """
    def __call__(self, response):
        return response
HTTPNullAuth=HTTPNullAuth() #it might as well be a singleton




# ------------ OAUTH --------------------

# This only implements the Web authorization flow, which is the most common one.
# There are three parties: user, provider, client.
# The goal is for user (a person) to grant client (a website or mobile app, possibly also the plugins like Windows Gadgets or Gnome Shell plugins) a capability to use user's data that is held by provider.
#  (note: the capability granted is just a string; it is up to *each* provider to maintain)
# In this flow:
# A) user clicks "sign in with provider.com" on client.com
# B) client.com grabs a temporary "request token" from provider.com, and uses it to generate
# C) client.com tells user "go to provider.com/auth/?response_type=code&client_id=<provider.com's pre-registered ID for client.com>"
#     (you can also tack on scopes=x,y,z here; a 'scope' is just a string, but the idea is each scope corresponds to a set of permissions as defined by provider.com, and each provider is *supposed* to but there is no way to enforce that they will list the scopes being granted to user before granting happens)
# D) provider.com asks user to grant or deny access. if granted:
# E) provider.com tells user "go to client.com/callback?code=<.....>"
# F) client.com takes the given code and goes to

# Question: why does OAuth require swapping the code for an access token? Why not just hand back an access token?
#  a) because >????
# --- you could maybe fix this with public key crypto, couldn't you?


class OAuth2Provider(OAuthProvider):
    """
    sub-base class for OAuth2 identity providers
    
    Holds the remote OAuth endpoints in {auth,token}_url,
    and at init stores the app_{id, secret} strings that you need to get by registering a developer account with the OAuth provider,
    and has the whoami() method which is called after OAuth completes to extract account details
     (because OAuth is an authorization not, directly, an authentication protocol)
    
    Naming matters! Your subclasses must match the names shared between oauth-dropins/bootstrap-social/, because it gets scraped to generate identifying strings.
     (but you can choose your own capitalization; they are .lower()ed before use)

    This clusterfuck is because OAuth couldn't just spec something like "OAuth is a RESTful protocol and it MUST live at site.com/oauth/". bastards.
     each site's URLs have to be researched, and kept up to date,
     and further each site has to have custom code for extracting identity information once you've got an auth token.
    
    # TODO:
    # set up auto-token-refresh https://requests-oauthlib.readthedocs.org/en/latest/oauth2_workflow.html#third-recommended-define-automatic-token-refresh-and-update
    """
    auth_url = None
    token_url = None
    scope = None
    
    @classmethod
    def handle(self):
        """
        An endpoint which handles client (i.e. application) side OAuth.
        
        To login, a user GETs this endpoint.
        Then we look up the proper provider in our backend and redirect them over there.
        The provider (should) asks the client to auth us, and if so redirects them back
        to us *at the same endpoint* (which is unusual: most OAuth flows have separate endpoints, one for the initial click, one for the callback, and one for the finishing step)
        
        """
        provider = self #HACK

        app_id, app_secret = self._credentials()
        
        # This code adapted from https://requests-oauthlib.readthedocs.org/en/latest/examples/facebook.html
        
        # An OAuth "app" is an account stored at provider which consists of
        #  at least (name, app_id, app_secret, callback)
        # redirect_uri is callback_uri, and it is where.
        # Providers have different rules about what callback is; Github wants it to be a prefix of a URI that you'll send as redirect_uri
        # Facebook lets you leave it blank but makes you fill in a domain name and checks that at least that matches.
        # 
    
        # We want this one endpoint to handle *all* the callbacks for all the providers,
        # so we set redirect_uri to request.url (e.g. https://localhost:5000/oauth2/facebook)
        # but we need it normalized because during the callback we get something like https://localhost:5000/oauth2/facebook?code=AQABl4ziZe9QsS1ZmT9QS1K5gZFV88M7YD5F0jGHcfuFKxFAF1QvqamERgXYSyHfYSFwnyrcyvmx1lQnJPMucUVI0VDr4OQIbHDafsnGKed65A6OLWbgH5SxQIu--IWC14bDvUMeIP5QcXgHKa5RTG755YqDGBDSn9fUxI_RioLrwzLyMiSad1E2ygK4Slofh6P0gcKZ4GDvAnaQHLFrBDhtZ7o-w-Wgv2VWkdjvsrrS75uFfa0-Ms_Cbg8-tLXJO7FGvfMJRZ1fJZo6x5_l0C3SvVIdNthwEf4T_Z0Ya7bg4dK9MHnHkTijhiETIHBvebwTRFm3FTgMB1R5BBqk8fax&state=64z2IR2IYZJ1l96DckFgmnNbnJcVjQ
        # which is technically wrong, certainly wasteful, and actively pisses off at least Facebook who says "this URL doesn't match!" and fails the fetch_token()
        S = OAuth2Session(app_id,
                          redirect_uri=urlstrip(request.url),
                          scope=provider.scope)
        
        # autoload compliance fix for the provider, if it exists
        S = getattr(requests_oauthlib.compliance_fixes, '%s_compliance_fix' % (provider,), lambda e: e)(S)
        
        # We figure out if we're the initial click or a callback
        if request.args.get("state") is None:
            # Initial click
            # construct the session-specific auth url at the provider to send the user over to
            auth_url, session['oauth_state'] = S.authorization_url(provider.auth_url)
            return redirect(auth_url)
        else:
            # Callback! Fetch a token!
            S._state = session.pop('oauth_state')
            
            # NOTICE: we *don't* reload the state from the query string, instead it's from the session
            # this protects against CSRF because 
            # (the actual check is buried in oauthlib.oauth2.rfc6749.parameters.parse_authorization_code_response())
            token = S.fetch_token(provider.token_url, authorization_response=request.url, client_secret=app_secret, client_id=app_id, auth=HTTPNullAuth)
            
            try:
                return provider.whoami(S)
            except:
                return "You suck", 500

