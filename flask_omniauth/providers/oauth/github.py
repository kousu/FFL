from .oauth import OAuth2Provider
from ... import users

class Github(OAuth2Provider):
    #  # https://developer.github.com/v3/oauth/
    auth_url = 'https://github.com/login/oauth/authorize'
    token_url = 'https://github.com/login/oauth/access_token'
    
    name = "GitHub"
    icon = "github"
    
    @staticmethod
    def link(id):
        return "https://github.com/%s" %(id,)

    @staticmethod
    def whoami(session):
        """

        returns:
        id: numeric account ID
        name: freeform real-name
        login: username (note! github allows changing this! so treat it like a freeform name!)
        """
        profile = session.get("https://api.github.com/user").json()
        current_app.logger.debug("Received this profile from github:\n--------------\n%s\n--------------", pformat(profile))
        return users.User("github", profile['login'], profile['name'], profile['avatar_url']+"&s=50") # the &s=50 makes github resize the picture to 50x50 before replying (this matches the only size Facebook will give out)
