from .oauth import OAuth1Provider
from ... import users

class Tumblr(OAuth1Provider):
    """
    Register at https://www.tumblr.com/oauth/apps
    Review https://www.tumblr.com/docs/api_agreement before registering.
    """
    request_url = 'http://www.tumblr.com/oauth/request_token'
    auth_url = 'http://www.tumblr.com/oauth/authorize'
    token_url = 'http://www.tumblr.com/oauth/access_token'
    
    name = "Tumblr"
    icon = "tumblr"

    @staticmethod
    def link(id):
        return "https://%s.tumblr.com/" %(id,)
    
    @staticmethod
    def whoami(session):
        # https://www.tumblr.com/docs/en/api/v2#user-methods
        r = session.get("https://api.tumblr.com/v2/user/info")
        r.raise_for_status()
        profile = r.json()['response']['user']
        # tubmlr accounts don't have avatars, *blogs* do
        # but each account has a *primary* blog whose avatar could in theory be extracted
        # also, the user info thing only gives a username, not an id, and I am 99% sure this can be easily changed...
        # but you work with what you've got
        # actually, since "identity" on tumblr basically comes down to <x>.tumblr.com and identities are supposed to be easily dumpable
        
        # find the avatar of the primary blog (this always exists; tumblr generates one if not set)
        app.logger.debug("GOT THIS PROFILE FROM TUMBLR:")
        pprint(profile)
        blog = [b for b in profile['blogs'] if b['primary']]
        if len(blog) != 1:
            raise Exception("Tumblr gave %d primary blogs for %d, but there should only every be 1." % (len(blog),profile['name']))
        blog = blog[0]
        
        # https://www.tumblr.com/docs/en/api/v2#blog-avatar
        # you can ask for different sizes by appending the pixels with /[pixels], e.g. avatar/512 gets the largest avatar
        # the default is a smallish
        avatar = session.get("https://api.tumblr.com/v2/blog/%(blog)s.tumblr.com/avatar" % {"blog": blog['name']})

        
        # tumblr doesn't give us user ids, so use the login as the userid
        # (which tbh I'd prefer to do for everyone, but the way that names can be thrown away makes me think twice..)
        # oh and for the avatar, we don't actually care about having the image data just yet, just the link is enough, so we can use .url:
        #                           id,           username,    name, avatar
        return users.User('tumblr', profile['name'], blog['name'], avatar.url)
