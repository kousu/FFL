from .oauth import OAuth2Provider
from ... import users

class Google(OAuth2Provider):
    """
    https://developers.google.com/identity/protocols/OAuth2
    
    Get keys at https://console.developers.google.com/#identifier
    Note that Google enforces that callback_url match the registered one, so you must set it right
    which makes deployment tedious
    
    # USEFUL: https://developers.google.com/oauthplayground
    # also https://developers.google.com/identity/protocols/googlescopes
    """
    auth_url = "https://accounts.google.com/o/oauth2/auth"
    token_url = "https://accounts.google.com/o/oauth2/token"
    scope = ["https://www.googleapis.com/auth/userinfo.profile"]
    
    name = "Google"
    icon = "google"

    #@staticmethod
    #def link(id):
    #   #???
    #   return "https://google.com/%s" %(id,)

    
    @classmethod
    def whoami(self, session):
        # TODO: switch to /v3
        profile = session.get('https://www.googleapis.com/oauth2/v1/userinfo')
        profile.raise_for_status()
        profile = profile.json()
        
        # 'email' is only in profile if we ask for userinfo.email

        return users.User(self.icon, profile['id'], profile['name'], profile['picture'])
