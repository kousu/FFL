
from __future__ import absolute_import
from __future__ import print_function

import sys
import re
import binascii, hashlib
if sys.version_info[0] == 3:
    from urllib.parse import *
elif sys.version_info[0] == 2:
    from urlparse import *
else:
    raise ImportError # or VersionError?
from subprocess import Popen

from flask import *

from .base import Provider
from .. import users

# this code is the worst I've written in a long time
# but it does the thing it supposed to

# fernet is like its dangerous except it also encrypts
from cryptography.fernet import Fernet, InvalidToken

#fernet = Fernet(Fernet.generate_key()) #<-- note: we're using a *different* key than the app key. Fernet is picky about key sizes (unlike it's dangerous)
# XXX
k = b'pZZNksB_QZz0JIzh1mUl9Kc4lKatmj4QLMpXfxwVyNE='
fernet = Fernet(k) #<-- note: we're using a *different* key than the app key. Fernet is picky about key sizes (unlike itsdangerous) # XXX having two keys to configure is a terrible idea.
# better idea: use a Key Derivation Function to turn app.secret_key into a fernet key

import os
import pkg_resources
# XXX I want pkg_resources to tell me the path to flask_omniauth. but maybe that's not possible?
EMAIL = os.path.abspath(os.path.join(pkg_resources.get_provider(__package__).module_path, "..", "./email"))

# FIXME: this could probably be in ..util, but it only makes sense to be used on email accounts and the imports are simpler if it's just kept safely here.
def gravatar(userid):
    # https://en.gravatar.com/site/implement/hash/
    # gravatars are *supposed* to be a hash of an email address
    # but i'm explicitly not relying on knowing email addresses
    # so just generate *some* hash and use that
    hash = binascii.hexlify(hashlib.sha256(userid.encode("utf-8")).digest()).decode("ascii")
    return "https://www.gravatar.com/avatar/%s.jpg" % (hash,)

class Email(Provider):
    """
    An email address identity: you prove you own the address by receiving a token to it.

    The only way to change your display name is to revalidate your email address.
    """
    # TODO:
    # - there's flask-mail that can handle this
    # - we could also just shell out to mail(1) and insist that your sysadmin has to configure email; that's the most unixy way to do it, and it's
    
    # to prove you own an email, we send a token to that address and you paste it back to us,
    # either by clicking a link with the token embedded or by copy-pasting it directly at us
    
    # Q: should I use TOTP for this too, or something else?
    # I could use this scheme:
    #  - generate a json string that says {"id": <...>}, sign it with itsdangerous, and maybe encrypt it with AES just to keep prying eyes away.
    #  - when the user presents this, we grab the id back out
    # TOTP is tricky to do statelessly.
    # to do TOTP we would have to include the email you're authing as in the, and I would have to think through that to make sure it's secure
    
    name = "Email"
    icon = "envelope"

    AUTH_WINDOW = 2*24*60 # 2 days
    #AUTH_WINDOW = 40 #40 seconds DEBUG
    
    
    @staticmethod
    def normalize_id(id):
        m = re.compile(r"\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b", re.I).match(id)
        if not m:
            raise ValueError("Invalid email address")
        return id
    
    @staticmethod
    def link(email):
        return "mailto:%s" % (email,)
    

    # this code is soooo bad
    # ugh
    # fuck youuuu Flask
    # what can I do instead?? keep a global dictionary of coroutines in process? that means holding onto state, and that's scary.
    # plus Flask doesn't assign session IDs (but Flask-Login does) so i'd have to roll my own somehow.
    #
    
    @classmethod
    def handle(self):
        provider = self #HACKS
        
        if request.method=="GET" and 't' not in request.args:
            # initial display
            return render_template("flask_omniauth/login_email.html")
        
        elif request.method=="POST":
            
            token = {'id': request.form['id'], 'name': request.form['name']} #request.form is an ImmutableMultiDict, so just casting it gives {'id': [<id>], 'name': [<name>]} because each element could potentially be a list
            # but it special-cases  to behave like a normal dict in the common case of a single element
            # but if we're gonna be serializing this shit
            token['id'] = self.normalize_id(token['id']) #TODO

            # dict -> str -> bytes -> crypted bytes -> str
            # the first step uses utf-8 because we could have any characters, but the last step uses ascii because fernet base64s stuff
            # XXX is using json a security hole? It means that there's multiple encodings with the same meaning.
            token = fernet.encrypt(json.dumps(token).encode("utf-8")).decode("ascii")
            
            id = request.form['id']
            subject = "Email Verification"
            msg = "Verify your email address at:\r\n\r\n%s" % ("%s?%s" % (request.url, urlencode({"t": token}),))
            
            Popen([EMAIL, id, subject, msg])
            
            return render_template("flask_omniauth/login_email.html", mode="sent", id=id)
        
        elif request.method=="GET" and 't' in request.args:
            
            try:
                # extact the token
                # implicitly, since fernet signs the token, no one can construct one of these except for us
                # now, someone *could* replay, but maybe replays aren't so terrible a threat, especially if we don't keep account state
                # str -> crypted bytes -> bytes -> str -> dict
                token = json.loads(fernet.decrypt(request.args['t'].encode("ascii"), ttl=self.AUTH_WINDOW).decode("utf-8"))
                
                #access fields to ensure they exist
                token['id']
                token['name']
            except (InvalidToken, KeyError):
                flash("Email verification failed. Perhaps your token has expired.")
                return redirect(url_for("login"))
            
            return users.User(self.__name__.lower(), token['id'], token['name'], avatar=gravatar(token['id']))
