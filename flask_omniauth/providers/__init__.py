"""

The API should be that
> from .providers import *
gives all the providers


Note: a bunch of these, especially the OAuth providers, are untested,
out of laziness and the difficulty there. Code has been moved around,
so if stuff isn't importing right: it's my fault, not yours, but please
fix it if you can.

I can probably, with effort, make mock OAuth1 and OAuth2 providers,
and mock SMS and Email recepients, but fully testing Facebook, Tumblr
and Google and all the rest requires having active developer keys with
them, and I'm just not into that.

TODO:
I'd like to pull apart users and login from the providers
it's proving annoying that the providers are tied to ..users
and while we're at it, annoying that they're tied to flask_login.
Maybe login_user() should be called from omniauth.login();
then the providers don't depend on flask_login
though they do depend on the User class which does inherit from flask_login.UserMixin
so they're not totally independent
It's probably impossible to make them totally independent

TODO:
- split whoami() and verify() out of handle()
  some providers will still need a complicated handle(),
  but for the most part maybe we can isolate the moving parts

"""

from __future__ import absolute_import
from __future__ import print_function

# Pull in all the auth providers in this subpackage.
# Notice: we *don't* import oauth.py or base.py
#
# FIXME: you should be able to use the parts of omniauth you want
# without the parts you don't; but this setup will fail if you
# don't have ALL of the dependencies installed; I'll have to find the better approach.
# 
# Maybe it would be better to walk listdir(dirname(__file__)), import everything (with importlib),
# and then filter __all__ to only contain things we were actually able to import.
# Or maybe we should do the imports when we read the omniauth config that actually defines which providers we need
# hm.
from .local import Local
from .sms import SMS
from .email import Email
from .openid import OpenID
from .oauth.facebook import Facebook
from .oauth.github import Github
from .oauth.google import Google
from .oauth.tumblr import Tumblr
from .oauth.twitter import Twitter
from .pseudoanon import Pseudoanon

# The list of exports is also the default preferred ordering of login backends
# Commented out items are TODOs
__all__ = [
  ## Local
  'Local',
  #'LDAP',
  #'PAM',
  #'CAS',

  #'loginless',    # this is my implementation of:
  #'passwordless', # https://github.com/kevinjqiu/flask-passwordless / https://passwordless.net
  # aka token login, where you simply send a cryptographically signed token with an *expiry date*
  # my idea is that your token is bookmarkable, which means it needs to be long-lasting
  # 'passwordless' is more like 'Email' or 'SMS', but it seems to use a database rather than cryptographically signing to verify its contents; the advantage of crypto is you only need to keep the app key around; whereas rather with a database you need to store every user that tries to sign up.

  ## Challenge-based
  'Email',
  'SMS',
  # TODO: either make a series of backends, SMS_Vitelity, SMS_Clickatell, SMS_Twilio, SMS_VoipMS, ..., or insist that it's the user's job to provide a "sms" script on the PATH that can send.
  #'XMPP'

  ## Federated Web
  # OpenID
  'OpenID',
  #'SAML',
  # OAuth2
  'Facebook',
  'Github',
  'Google',
  # OAuth1
  'Tumblr',
  'Twitter',

  ## Also local,
  #  but at the end because no one wants to use it
  'Pseudoanon',
]
