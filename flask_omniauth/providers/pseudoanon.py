
from __future__ import absolute_import
from __future__ import print_function

from uuid import uuid4

from flask import *

from .base import Provider
from .. import users

WORDLIST = "/usr/share/dict/words" #XXX hardcoded. this is pretty standard, but will need cross-platform patching here and there.

class Pseudoanon(Provider):
    # TODO
    # this is the pseudoanonymous option, where there is no way to prove
    # my idea is: click once on a subscribe link, possibly fill in a profile (that is, name and avatar, but not login!) and an account is generated for you, along with an auth token
    # then to get back in you must click the auth token link. there's no username or password
    
    with open(WORDLIST) as o:
        #current_app.logger.info("Loading wordlist %s", WORDLIST)
        _words = list(set(e.strip().lower().split("'")[0] for e in o)) #TODO: stem the words
        if len(_words) < 2**16:
            raise ValueError("Not enough words in '%s' for us: we assume 16 bits worth (65536) of words." % (WORDLIST,))
    
    
    name = "Pseudoanomity"
    icon = "barcode" #"asterisk"?

    @staticmethod
    def link(email):
        return "pseudoanon:%s" % (email,)
    
    
    @classmethod
    def genid(self):
    	# the way we generate an id is by picking one word
    	# but we ride on the back of uuid to do this, in
    	# hopes that it is cryptographically secure
    	# where random.choice() probably isn't.
        id = uuid4().bytes
        
        # chunk id into 16-bit blocks, which 65536 words to choose from
        # (and all those xes and zes get cut. too bad for them)
        id = [(id[i]<<8)|id[i+1] for i in range(0,len(id),2)]
        id = [self._words[c] for c in id]
        id = "-".join(id)
        
        return id
        
    
    @classmethod
    def handle(self):
        if request.method == "GET":
            # render a form
            session['partial_pseudoanon_id'] = self.genid() #securely generate an id serverside; don't let the user choose!
            return render_template("flask_omniauth/login_pseudoanon.html", id=session['partial_pseudoanon_id'], action=request.url)
        elif request.method == "POST":
            # FIXME: if the user loses their cookie between GETing and POSTing this will crash
            # because it doesn't know what the generated ID was
            # this should be handled better

            # read form results
            id = session.pop('partial_pseudoanon_id')
            return users.User(self.__name__.lower(), id, display_name=request.form['name'])
