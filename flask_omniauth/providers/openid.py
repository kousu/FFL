
from __future__ import absolute_import
from __future__ import print_function

import openid.consumer.consumer

from flask import *

from .base import Provider
from .. import users

class OpenID(Provider):
    name = "OpenID"
    icon = "openid"
    
    @staticmethod
    def link(id):
        return id #your id *is* a link with OpenID
    
    @classmethod
    def handle(self):
        if request.method == "GET" and len(request.args)==0:

            return render_template("flask_omniauth/login_openid.html", action=request.url)
        elif request.method == "POST":
            # request
            
            sid = Fernet.generate_key()
            client = openid.consumer.consumer.Consumer({"id": sid}, None)
            
            realm = request.url
            callback_url = request.url
            auth_url = client.begin(request.form['id']).redirectURL(request.url, callback_url)

            # this is state that needs to be saved and restored for OpenID to behave itself
            session['openid_session'] = sid
            session['openid_callback'] = callback_url

            return redirect(auth_url)

        else:
            # response 
            assert len(request.args) > 0 and request.method == "GET"
            client = openid.consumer.consumer.Consumer({"id": session.pop('openid_session')}, None)
            
            # python-openid isn't as smooth as python-requests-oauthlib, so we need to do a bit of pre-parsing:
            resp = request.url
            resp = parse_qs(urlsplit(resp).query)
            resp = {k: v[0] for k,v in resp.items()} #strip the list (multi-valued items can fuck off good and well)
            
            verify = client.complete(resp, session.pop('openid_callback')) #argh, why?
            
            if isinstance(verify, openid.consumer.consumer.SuccessResponse):
                # TODO: OpenID has an extension (uh..OpenID Connect?) that allows asking for profile data on top of proving the URL
                return users.User(self.__name__.lower(), verify.identity_url)
            else:
                return "%s fail" % (verify.identity_url), 403
