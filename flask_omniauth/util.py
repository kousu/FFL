#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import print_function

import sys
import os.path

if sys.version_info[0] == 3:
    from urllib.parse import *
elif sys.version_info[0] == 2:
    from urlparse import *
else:
    raise ImportError # or VersionError?


def urlstrip(url):
    # return url # if you uncomment this, Facebook breaks because you end up sending two different redirect_uris to it. I'm hanging onto this for now because 
    "normalize a URL to just the REST-ful object path part (i.e. scheme, host, path)"
    (scheme, netloc, path, _, _, _) = urlparse(url.strip())
    # path = os.path.normpath(path) # TODO: decide if this is wanted; it changes the behaviour, obviously, I just don't know if that's what we want; if this is in, maybe this should be "urlnorm" instead of "urlstrip"
    return urlunparse((scheme, netloc, path, "", "", ""))


def chrooted(path):
    "prevent directory traversal by .... "
    return os.path.normpath(os.path.join("/", path)).lstrip("/")



if __name__ == '__main__':
    assert chrooted("/a/b/c") == "a/b/c"
    assert chrooted("a/b/c") == "a/b/c"
    assert chrooted("b/c") == "b/c"
    assert chrooted("a/../b/../../c/d/..") == "c"
    assert chrooted("/../../a") == "a"
    #assert chrooted("/../../a/") == "a/" # fails?

    assert urlstrip(" https://gmail.com:3242/login.lol/?abce    	") == "https://gmail.com:3242/login.lol/"
    assert urlstrip("ftp://example.com//a/b/c;akjnds    	") == "ftp://example.com//a/b/c"

    assert gravatar("") == "https://www.gravatar.com/avatar/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855.jpg"
    assert gravatar("abelincoln@whitehouse.gov") == "https://www.gravatar.com/avatar/edfe4c80e2186f758c5d8185adf1d43cfe973b3bba9f5e547eac010ab557fb83.jpg"

    print("Tests passed")
