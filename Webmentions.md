

Webmentions
===========

The webmention spec is a stripped down version of trackbacks.
trackbacks/pingbacks needed an XMLRPC endpoint to handle stuff, which sort of implied a running database and a bunch of boilerplate.

Webmentions are more RESTful
The `protocol http://indiewebcamp.com/webmention-spec` is:
0. there are two URLs: sender and receiver. sender has 'mentioned' (think "commented on" or "reblogged" -- note: requires each comment to have a permalink of its own, which is not that unusual these days, but by no means universal or easy) receiver and wants to notify him.
1. Discovery: sender GETs https://receiver-site/postname and scans the HTML for <link rel="webmention">. If found, the href is taken as the "webmention endpoint".
2. Notification: sender POSTs http://receiver-site/webmention_listener {sender=<sender>, receiver=<receiver>}
3. Verification: receiver (asynchronously) GETs <sender> and scans the HTML for <receiver>.
4. (optional) receiver extracts microformats from the

This system lets anyone make the server hit any URL, simply by telling it lots of false URLs have mentioned one of its posts, which is a bandwidth amplification vuln.

This protocol is simpler on the wire, but it still uses endpoints and it still requires coordination.
If the protocol was *fully* RESTful then it would use the source and target URLs direct
I have a better design:
 Replace 1 and 2 with GET and Referrers:
   the webmention endpoint is *always* the receiver itself
   and notification is sender GETs receiver with `Referrer: sender` 
- This is 100% compatible with all existing systems: you don't need to put up weird POST handlers, and there's no need for discovery because if the receiver doesn't do webmentions it'll just ignore it as if it was any other browser request
- this requires some middleware, or mod_webmention, or something. but that's not unreasonable. the backend server can be entirely static.
 if receiver knows about webmentions, it can at that point spawn a subprocess to check the incoming link for mentions
   and instead of thinking of it as verification, make step 4 non-optional and instead think of the reverse GET as *finding the content*: you don't count it as a webmention unless you discover the mention microformat on the reverse link with a link to you.
 now, using Referrer means that every visitor who follows the link outwards will trigger you to poll again. for example, everyone incoming visitor will make you GET the google search they just came from
   so maybe instead of Referrer, use an X-Webmention: header. That won't be triggered accidentally, still doesn't require

the DDoS prevention issues are identical to those email has had to deal with, though the risk is smaller because nothing gets automatically posted if the attacker gets through
Idea: can the mention be signed? to further reduce DDoS surface?
if you run sender.com how can receiver.com know the mention came from you?
 idea one: just check IP addresses: is the incoming address the same as the server claimed in X-Webmention: (similar to SPF; actually SPF is more restrictive: you have to manually specify in a txt record which servers are which; hmmm. but DNS has multiple)
 idea two: stick a public signature key into DNS and sign the webmention with it (similar to DKIM)
