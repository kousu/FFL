
### Unrelated work

There are a lot of people working on clawing the ideal of a decentralized net back from feudal control.

* https://storj.io/
* https://ipfs.io/
* https://geti2p.net/en/
* tor
* https://www.cs.cmu.edu/~xia/ / https://github.com/AltraMayor/XIA-for-Linux/