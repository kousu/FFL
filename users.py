"""
The User / Groups database contains everyone who has ever signed in.
It is very simple: it is a flat file database, one file per user, one folder per group
relative to the working directory
this means any program you use this user database in *cannot ever os.chdir()*

.users/{id}.yml
.users/Groups/{Group}.yml -- which 
or .users/Groups/{Group}/{id.yml} -- these can be symlinks to one level up?
 we need to have users in some kind "Requests" limbo
 maybe this can just be a .users/Groups/Purgatory.yml ? Every new user ends up in there.
 or maybe it should be .users/Purgatory/${id}.yml

 later:
 are there more efficient ways to do the ACL calculation?
 like, check the explicit minuses first?

or .users/group:{Groupname}.yml, which flows it in almost with the rest
"""

from __future__ import absolute_import
from __future__ import print_function

"""
TODO:

* [ ] use os.flock() to protect concurrency
* [ ] wrap this all up to look like dictionaries
   Users['sms:+15552224444'] = ... etc
   Groups['Friends'].append(Users['sms:+15552224444'])
* [ ] Record os.getcwd() at import time to enable os.chdir()?

idea:
 groups could instead be in their own provider-like namespace "group:"
 the advantage of that would be in simplifying the API (you just say "facebook:1941,email:b@example.com,group:friends,-group:acquaintances")

"""

import sys, os
from os.path import dirname, basename, join, exists
from glob import glob

if sys.version_info[0] == 2:
    # polyfill version differences
    # TODO: is it easier to simply use futurize?
    # it's the tradeoff between the weight of a dependency vs the weight of writing the polyfills
    
    import errno
    class FileNotFoundError(Exception): pass
    
    _open = open
    def open(*args, **kwargs):
        try:
    	    return _open(*args, **kwargs)
        except IOError as exc:
            if exc.errno == errno.ENOENT:
            	raise FileNotFoundError("... FIXME ...") # FIXME
            raise
    
    _unlink = os.unlink
    def unlink(*args, **kwargs):
        try:
    	    return _unlink(*args, **kwargs)
        except OSError as exc:             # it's weird that this one gives OSError and that gives IOError
            if exc.errno == errno.ENOENT:  # ; I guess that's why they cleaned it up in python3
            	raise FileNotFoundError("... FIXME ...") # FIXME
            raise
    os.unlink = unlink
    del unlink
    	

import yaml

from flask_omniauth import users

# XXX I'm not sure how much I want to bind this file to flask
# it seems like, except for this, we don't need flask
# flask uses 'blinker' internally for this
# so maybe we could just use that directly?
# though that will probably bite us in other ways
# 
from flask import current_app
from flask.signals import _signals
user_first_login = _signals.signal('first-login')

from flask_omniauth.util import chrooted # <--- layering leak


def user_save(user):
    "persist user to the database"
    path = chrooted(join(".users/", user.id))
    if not exists(dirname(path)):
        os.makedirs(dirname(path))

    first_login = not exists(path)
    with open(path, "w") as p:
        p.write(yaml.dump(user))
    if first_login:
        # this has to happen after the write so that listeners can assume the user exists
        # which means we have this awkward state-saving temp var first_login here :/
        user_first_login.send(current_app._get_current_object(), user=user)



def user_load(userid):
    "load a User object out of the database"
    path = chrooted(join(".users/", userid))
    try:
        with open(path, "r") as p:
            return yaml.load(p.read()) # TODO: yaml. because fuck json.
    except FileNotFoundError:
        raise KeyError(userid)


def user_del(user):
    "remove a user from the databse"

    if hasattr(user, 'id'):
        userid = user.id
    else:
        userid = user

    path = chrooted(join(".users/", userid))
    # XXX this needs to also del from all the groups
    try:
        os.unlink(path)
    except FileNotFoundError:
        raise KeyError(userid)
    
    
    # del user from all groups
    search = chrooted(join(".users/Groups/*/", userid))
    search = glob(search)
    for f in search:
    	os.unlink(f)
    	# this doesn't handle exceptions because, hopefully, glob() has only found legitimate files we can really delete.
    	# maybe that's a silly assumption.


def add_group(group):
    path = chrooted(join(".users/Groups", group,))
    if not exists(path):
        os.makedirs(path)


def del_group(group): #XXX badly named
    "delete a group"
    # we just have to delete the directory for a group to clean it up
    path = chrooted(join(".users/Groups", group))
    if exists(path):
        os.unlink(path)
    else:
        raise KeyError("%s" % (group,))
    

def group_add(group, user):
    if hasattr(user, 'id'):
        user = user.id
    # check if user exists (this will KeyError if not)
    user_load(user)
    
    add_group(group)

    # FIXME: don't use system()!!! securrrrrity / dependencies!
    path = chrooted(join(".users/Groups", group, user))
    os.system("touch '%s'" % (path,))


def group_del(group, user):
    if hasattr(user, 'id'):
        user = user.id
    path = chrooted(join(".users/Groups", group, user))
    if exists(path):
        os.unlink(path)
    else:
        raise KeyError("%s does not contain %s" % (group, user))



def members(group):
    search = chrooted(join(".users/Groups/", group, "*"))
    search = glob(search)
    found = [basename(f) for f in search]
    return set(found)


def groups(user):
    if hasattr(user, 'id'):
        user = user.id
    # search
    # ...glob???
    search = chrooted(join(".users/Groups/*/", user))
    search = glob(search)
    # TODO: check this for security holes, etc
    found = [basename(dirname(f)) for f in search]
    return set(found)


if __name__ == '__main__':
    import tempfile
    os.chdir(tempfile.mkdtemp()) # isolate our tests to a separate folder
    
    U1 = User('facebook', '32423432432', "Saladin Smartacks")
    U2 = User('sms', '+15553334444', "Audrey Nevermore")
    
    user_save(U1)
    assert exists(".users/facebook:32423432432")
    
    U1_ = user_load("facebook:32423432432")
    assert U1 == U1_
    
    try:
        user_del(U2)
        # this should error out because U2 hasn't been saved yet
        assert False, "NotReached"
    except KeyError:
        pass
    except:
        # anything else is bad
        raise
    
    # the same as before but using the userid directly   
    try:
        user_del("sms:+15553334444")
        # this should error out
        assert False, "NotReached"
    except KeyError:
        pass
    except:
        # anything else is bad
        raise
    
    assert groups(U2) == set()
    try:
        assert members("Friends") == set()
        #assert False, "NotReached"
    except KeyError:
        pass
    except:
        # everything else is bad
        raise
    
    # test_fail_group_adding
    try:
        # this should fail because U2 doesn't exist
        group_add("Friends", U2)
    except KeyError:
        pass
    except:
        # everything else is bad
        raise
    
    
    user_save(U2)
    
    
    # test_group_adding
    group_add("Friends", U1)
    group_add("Friends", U2)
    group_add("Frenemies", U2)
    
    assert groups(U1) == {"Friends"}
    assert groups(U2) == {"Friends", "Frenemies"}
    
    os.system("echo -n \"Results in \"; pwd; ls -l `ls -A`/* `ls -A`/*/*")
    print("-"*80)
    print()
    
    # test_group_deleting
    group_add("Hackme", U2)
    assert groups(U2) == {"Friends", "Frenemies", "Hackme"}
    assert members("Hackme") == {U2.id}
    group_del("Hackme", U2)
    assert groups(U2) == {"Friends", "Frenemies"}
    assert members("Hackme") == set()
    
    os.system("echo -n \"Results in \"; pwd; ls -l `ls -A`/* `ls -A`/*/*")
    print("-"*80)
    print()
    
    # test_user_deleting
    user_del(U2)
    assert members("Friends") == {U1.id}
    assert members("Frenemies") == set()
    
    # DEBUG: just print the resulting files
    os.system("echo -n \"Results in \"; pwd; ls -l `ls -A`/* `ls -A`/*/*")
    #os.system("rm -r .users/")

    print()
    print("Tests passed")
